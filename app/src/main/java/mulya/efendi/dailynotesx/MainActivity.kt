package mulya.efendi.dailynotesx

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.BaseAdapter
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.row.*
import kotlinx.android.synthetic.main.row.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class MainActivity : AppCompatActivity(), View.OnClickListener{
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnDel -> {
                queryInsertUpdateDelete("delete")
            }
        }
    }

    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"
    //background
    var FONT_TITLE_COLOR = "bg_color"
    val DEF_FONT_TITLE_COLOR = "#E8E8E8"

    lateinit var notesAdapter: AdapterDataNotes
    var daftarNotes = mutableListOf<HashMap<String,String>>()
    var mSharedPref:SharedPreferences?=null
    val url = "http://192.168.43.77/uas-2d-daily-notes-web/show_data_asc.php"
    val url2 = "http://192.168.43.77/uas-2d-daily-notes-web/show_data_desc.php"
    val url3 = "http://192.168.43.77/uas-2d-daily-notes-web/query_ins_upd_del.php"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mSharedPref = this.getSharedPreferences("My_Data", Context.MODE_PRIVATE)

        val mSorting = mSharedPref!!.getString("Sort", "asc")
        when(mSorting){
            "asc" -> showDataAsc()
            "desc" -> showDataDesc()
        }

        showDataAsc()

        notesAdapter = AdapterDataNotes(daftarNotes,this)
        listNote.layoutManager = LinearLayoutManager(this)
        listNote.adapter = notesAdapter

        btnDel.setOnClickListener(this)

    }
    

    fun loadSetting(){
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        listNote.setBackgroundColor(Color.parseColor((preferences.getString(FONT_TITLE_COLOR,DEF_FONT_TITLE_COLOR))))
        cLayout.setBackgroundColor(Color.parseColor((preferences.getString(FONT_TITLE_COLOR,DEF_FONT_TITLE_COLOR))))
    }

    override fun onResume() {
        super.onResume()
        val mSorting = mSharedPref!!.getString("Sort", "asc")
        when(mSorting){
            "asc" -> showDataAsc()
            "desc" -> showDataDesc()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onStart() {
        super.onStart()
        loadSetting()
        showDataAsc()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item != null){
            when(item.itemId){
                R.id.addNote->{
                    startActivity(Intent(this, ActivityAddNote::class.java))
                }
                R.id.sortNote->{
                    showSortDialog()
                }
                R.id.settingNote->{
                    var intent = Intent(this,Setting::class.java)
                    startActivity(intent)
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun showSortDialog() {
        val sortOptions = arrayOf("Ascending", "Descending")
        val mBuilder = AlertDialog.Builder(this)
        mBuilder.setTitle("Sort by")
        mBuilder.setIcon(R.drawable.ic_action_sort)
        mBuilder.setSingleChoiceItems(sortOptions,-1){
            dialogInterface, i->
            if (i==0){
                Toast.makeText(this, "Ascending", Toast.LENGTH_SHORT).show()
                val editor = mSharedPref!!.edit()
                editor.putString("Sort", "ascending")
                editor.apply()
                showDataAsc()
            }
            if (i==1){
                Toast.makeText(this, "Descending", Toast.LENGTH_SHORT).show()
                val editor = mSharedPref!!.edit()
                editor.putString("Sort", "descending")
                editor.apply()
                showDataDesc()
            }
            dialogInterface.dismiss()
        }

        val mDialog = mBuilder.create()
        mDialog.show()
    }

    fun showDataAsc(){
        val request = StringRequest(Request.Method.POST,url,
            Response.Listener { response ->
                daftarNotes.clear()
                val jsonArray = JSONArray(response)
                for (x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var notes = HashMap<String,String>()
                    notes.put("id_note",jsonObject.getString("id_note"))
                    notes.put("title",jsonObject.getString("title"))
                    notes.put("nm_priority",jsonObject.getString("nm_priority"))
                    notes.put("nm_category",jsonObject.getString("nm_category"))
                    notes.put("url",jsonObject.getString("url"))
                    notes.put("description",jsonObject.getString("description"))
                    daftarNotes.add(notes)
                }
                notesAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataDesc(){
        val request = StringRequest(Request.Method.POST,url2,
            Response.Listener { response ->
                daftarNotes.clear()
                val jsonArray = JSONArray(response)
                for (x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var notes = HashMap<String,String>()
                    notes.put("id_note",jsonObject.getString("id_note"))
                    notes.put("title",jsonObject.getString("title"))
                    notes.put("nm_priority",jsonObject.getString("nm_priority"))
                    notes.put("nm_category",jsonObject.getString("nm_category"))
                    notes.put("url",jsonObject.getString("url"))
                    notes.put("description",jsonObject.getString("description"))
                    daftarNotes.add(notes)
                }
                notesAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Error to connect the server",Toast.LENGTH_LONG).show()
            })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(Method.POST,url3,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Delete success", Toast.LENGTH_LONG).show()
                    showDataAsc()
                }else{
                    Toast.makeText(this,"Delete failed.", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Can't connect to the server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
//                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
//                    .format(Date())+".jpg"
                when(mode){
                    "delete" ->{
                        hm.put("mode","delete")
                        hm.put("id_note",edId.text.toString())
                        hm.put("title",edTN.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

}
