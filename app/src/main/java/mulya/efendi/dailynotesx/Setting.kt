package mulya.efendi.dailynotesx

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_setting.*

class Setting : AppCompatActivity(), View.OnClickListener {

    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"
    //background
    var FONT_TITLE_COLOR = "bg_color"
    val DEF_FONT_TITLE_COLOR = "#E8E8E8"
    var FontTitleColor: String = ""

    override fun onClick(v: View?) {
        preferences = getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE)
        val prefEditor = preferences.edit()
        prefEditor.putString(FONT_TITLE_COLOR,FontTitleColor)
        prefEditor.commit()
        Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show()
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)

        radioGroup.setOnCheckedChangeListener { group, checkedId ->  
            when (checkedId) {
                R.id.rbBlue -> FontTitleColor = "#3498DB"
                R.id.rbGray -> FontTitleColor = "#E8E8E8"
            }
        }

        preferences = getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE)
        var rgselection = preferences.getString(FONT_TITLE_COLOR, DEF_FONT_TITLE_COLOR)
        if (rgselection == "#3498DB"){
            radioGroup.check(R.id.rbBlue)
        } else{
            radioGroup.check(R.id.rbGray)
        }
        btnSave.setOnClickListener(this)
    }

}