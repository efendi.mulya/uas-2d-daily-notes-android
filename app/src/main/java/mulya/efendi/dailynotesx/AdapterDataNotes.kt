package mulya.efendi.dailynotesx

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*

class AdapterDataNotes(val dataNotes: List<HashMap<String,String>>,
                       val mainActivity: MainActivity) :
    RecyclerView.Adapter<AdapterDataNotes.HolderDataNotes>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterDataNotes.HolderDataNotes {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row,parent,false)

        return HolderDataNotes(v)
    }

    override fun getItemCount(): Int {
        return dataNotes.size
    }

    override fun onBindViewHolder(holder: AdapterDataNotes.HolderDataNotes, position: Int) {
        val data = dataNotes.get(position)
        holder.txId.setText(data.get("id_note"))
        holder.txTitle.setText(data.get("title"))
        holder.txPriority.setText(data.get("nm_priority"))
        holder.txDesc.setText(data.get("description"))
        holder.txCategory.setText(data.get("nm_category"))

        holder.LineLayout.setOnClickListener(View.OnClickListener {
            mainActivity.edId.setText(data.get("id_note"))
            mainActivity.edTN.setText(data.get("title"))
        })

        if (!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(holder.photo);
    }

    class HolderDataNotes(v : View) : RecyclerView.ViewHolder(v){
        val txId = v.findViewById<TextView>(R.id.idNote)
        val txTitle = v.findViewById<TextView>(R.id.titleTv)
        val txPriority = v.findViewById<TextView>(R.id.priorityTv)
        val txDesc = v.findViewById<TextView>(R.id.descTv)
        val txCategory = v.findViewById<TextView>(R.id.katTv)
        val photo = v.findViewById<ImageView>(R.id.imageView)
        val LineLayout = v.findViewById<LinearLayout>(R.id.LineLayout)
    }

}