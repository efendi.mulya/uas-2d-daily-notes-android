package mulya.efendi.dailynotesx

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.activity_add_note.*
import kotlinx.android.synthetic.main.row.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class ActivityAddNote : AppCompatActivity(), View.OnClickListener {

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imUpload ->{
                requestPermissions()
            }
            R.id.addBtn ->{
                queryInsertUpdateDelete("insert")
            }
        }
    }

    lateinit var mediaHelper: MediaHelper
    lateinit var prioritasAdapter : ArrayAdapter<String>
    lateinit var kategoriAdapter : ArrayAdapter<String>
    var daftarPrioritas = mutableListOf<String>()
    var daftarKategori = mutableListOf<String>()
    val url1 = "http://192.168.43.77/uas-2d-daily-notes-web/get_nama_prioritas.php"
    val url2 = "http://192.168.43.77/uas-2d-daily-notes-web/get_nama_kategori.php"
    val url3 = "http://192.168.43.77/uas-2d-daily-notes-web/query_ins_upd_del.php"
    var imstr = ""

    var namafile = ""
    var fileUri = Uri.parse("")
    var pilihPrioritas = ""
    var pilihKategori = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_note)

        prioritasAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line, daftarPrioritas)
        spPrioritas.adapter = prioritasAdapter
        spPrioritas.onItemSelectedListener = itemSelected
        kategoriAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line, daftarKategori)
        spKategori.adapter = kategoriAdapter
        spKategori.onItemSelectedListener = itemSelected1

        try {
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        mediaHelper = MediaHelper()
        imUpload.setOnClickListener(this)
        addBtn.setOnClickListener(this)
    }
    override fun onStart() {
        super.onStart()
        getNamaPrioritas()
        getNamaKategori()
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spPrioritas.setSelection(0)
            pilihPrioritas = daftarPrioritas.get(0)
        }
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihPrioritas = daftarPrioritas.get(position)
        }
    }
    val itemSelected1 = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spKategori.setSelection(0)
            pilihKategori = daftarKategori.get(0)
        }
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihKategori = daftarKategori.get(position)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK)
            if (requestCode == mediaHelper.getRcCamera()){
                imstr = mediaHelper.getBitmapToString(imUpload,fileUri)
                namafile = mediaHelper.getMyFileName()
            }
    }

    fun requestPermissions() = runWithPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA){
        fileUri = mediaHelper.getOutputMediaFileUri()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT,fileUri)
        startActivityForResult(intent,mediaHelper.getRcCamera())
    }

    fun getNamaPrioritas(){
        val request = StringRequest(
            Request.Method.POST,url1,
            Response.Listener { response ->
                daftarPrioritas.clear()
                val jsonArray = JSONArray(response)
                for (x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarPrioritas.add(jsonObject.getString("nm_priority"))
                }
                prioritasAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->  }
        )
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
    fun getNamaKategori(){
        val request = StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                daftarKategori.clear()
                val jsonArray = JSONArray(response)
                for (x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarKategori.add(jsonObject.getString("nm_category"))
                }
                kategoriAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->  }
        )
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(Method.POST,url3,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Add note success", Toast.LENGTH_LONG).show()
                    finish()
                }else{
                    Toast.makeText(this,"Add note failed", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Can't connect to the server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                    .format(Date())+".jpg"
                when(mode){
                    "insert" ->{
                        hm.put("mode","insert")
                        hm.put("title",titleEt.text.toString())
                        hm.put("description",descEt.text.toString())
                        hm.put("image",imstr)
                        hm.put("file",nmFile)
                        hm.put("nm_priority",pilihPrioritas)
                        hm.put("nm_category",pilihKategori)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

}
